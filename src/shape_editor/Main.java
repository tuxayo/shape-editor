package shape_editor;

import javax.swing.*;

public class Main {
	public static void main(String[] args) {
		JFrame f = new JFrame("Editor");
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		ShapePanel shapePanel = new ShapePanel();
		f.getContentPane().add(shapePanel);
		f.pack();
		f.setVisible(true);

//		Shape rectangle = new Rectangle(new Point(20, 100), new Point(200, 400));
//		shapePanel.add(rectangle);
	}
}
