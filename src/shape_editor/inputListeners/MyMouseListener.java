package shape_editor.inputListeners;

import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import shape_editor.ShapePanelContext;

public class MyMouseListener extends MouseAdapter {

	private ShapePanelContext context;

	public MyMouseListener(ShapePanelContext context) {
		this.context = context;
	}

	@Override
	public void mousePressed(MouseEvent e) {
		Point point = new Point(e.getX(), e.getY());
		context.mousePressed(point);
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		Point point = new Point(e.getX(), e.getY());
		context.mouseDragged(point);
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		Point point = new Point(e.getX(), e.getY());
		context.mouseReleased(point);
	}
}
