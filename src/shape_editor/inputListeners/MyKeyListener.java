package shape_editor.inputListeners;

import java.awt.Point;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import shape_editor.ShapePanelContext;

public class MyKeyListener extends KeyAdapter {
    private ShapePanelContext context;

	public MyKeyListener(ShapePanelContext context) {
		this.context = context;
	}

	@Override
	public void keyTyped(KeyEvent e) {
		Point point = context.getShapePanel().getMousePosition(false); // Ceci n'est pas un false. // Private joke, I already forgot it, ignore
    	context.keyTyped(e.getKeyChar(), point);
    }
}
