package shape_editor;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFileChooser;
import javax.swing.JPanel;

import shape_editor.inputListeners.MyKeyListener;
import shape_editor.inputListeners.MyMouseListener;
import shape_editor.serializers.ShapeReader;
import shape_editor.serializers.ShapeWriter;
import shape_editor.shapes.Shape;

public class ShapePanel extends JPanel {
	private static final long serialVersionUID = -2835567974882467914L;

	private ShapePanelContext context;
	private List<Shape> shapes;

	public ShapePanel() {
		shapes = new ArrayList<>();
		context = new ShapePanelContext(this);

		setPreferredSize(new Dimension(800, 600));
		setFocusable(true);

		MyMouseListener mouseListener = new MyMouseListener(context);
		addMouseListener(mouseListener);
		addMouseMotionListener(mouseListener);
		addKeyListener(new MyKeyListener(context));
	}

	public void add(Shape shape) {
		shapes.add(shape);
		repaint();
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		paintAllShapes(g);
		context.paint(g);
	}

	private void paintAllShapes(Graphics g) {
		for(Shape s : shapes) {
			s.paint(g);
		}
	}

	public Shape getShape(Point p) {
		for (int k = shapes.size() - 1; k > 0; k--) {
			Shape s = shapes.get(k);
			if(s.contains(p))
				return s;
		}
		return null;
	}

	public void writeShapes() {
		File fileSaved = promptSaveFile();
		try (PrintWriter writer = new PrintWriter(fileSaved)) {
			ShapeWriter shapeWriter = new ShapeWriter(writer);
			shapeWriter.writeAll(shapes);
		} catch (FileNotFoundException e) {
			// TODO display a nice message to user
			System.err.println("Permission denied when trying to write file");
			e.printStackTrace();
		}
	}

	public void readShapes() {
		File fileToRead = promptOpenFile();

		try {
			tryReadShapes(fileToRead);

		} catch (FileNotFoundException e) {
			// TODO display a nice message to user
			System.err.println("Problem when trying to open the file, file not found or permission denied");
			e.printStackTrace();
		} catch (IOException e) {
			// TODO display a nice message to user
			System.err.println("Problem when reading the file");
			e.printStackTrace();
		}
	}

	private void tryReadShapes(File fileToRead) throws FileNotFoundException, IOException {
		FileReader fileReader = new FileReader(fileToRead);
		BufferedReader bufferedReader = new BufferedReader(fileReader);
		ShapeReader shapeReader = new ShapeReader(bufferedReader);

		try {
			shapeReader.readAllList(shapes);
			this.repaint();
		} finally {
			shapeReader.close();
		}
	}

	private File promptSaveFile() { // TODO refactor two methods (remove duplication)
		JFileChooser chooser = new JFileChooser();
		chooser.showSaveDialog(null);
		File fileSaved = chooser.getSelectedFile();
		return fileSaved;
	}

	private File promptOpenFile() {
		JFileChooser chooser = new JFileChooser();
		chooser.showOpenDialog(null);
		File fileSaved = chooser.getSelectedFile();
		return fileSaved;
	}
}
