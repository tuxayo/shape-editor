package shape_editor;

import java.awt.Graphics;
import java.awt.Point;

import shape_editor.states.CircleChoosenState;
import shape_editor.states.NullState;
import shape_editor.states.RectangleChoosenState;
import shape_editor.states.ShapeState;

public class ShapePanelContext {
	private ShapeState currentState;
	private ShapePanel panel;

	public ShapePanelContext(ShapePanel panel) {
		this.panel = panel;
		currentState = new NullState();
	}

	public void setState(ShapeState state) {
		this.currentState = state;
	}

	public ShapePanel getShapePanel() {
		return panel;
	}

	public void mousePressed(Point point) {
		currentState.mousePressed(this, point);
	}

	public void mouseReleased(Point point) {
		currentState.mouseReleased(this, point);
	}

	public void mouseDragged(Point point) {
		currentState.mouseDragged(this, point);
	}

	public void paint(Graphics graphics) {
		currentState.paint(this, graphics);
	}

	public void keyTyped(char character, Point point) {
		switch (character) {
		case 's':
			panel.writeShapes(); // TODO find why KeyEvent.VK_F12 failed
			break;
		case 'o':
			panel.readShapes();
			break;
		case 'c':
			setState(new CircleChoosenState());
			break;
		case 'r':
			setState(new RectangleChoosenState());
			break;
		case 'm':
//			setState(); TODO move state
			break;
		default:
			break;
		}

	}
}
