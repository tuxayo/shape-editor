package shape_editor.shapes;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;

import shape_editor.serializers.ShapeVisitor;
import shape_editor.util.MyMath;

public class Circle implements Shape {
	public Point center, moon;

	public Circle(Point center, Point moon) {
		this.center = center;
		this.moon = moon;
	}

	@Override
	public void paint(Graphics g) { // TODO refactor: remove duplication with the state
		Point tl = topLeftCorner();
		int d = diameter();
		Graphics2D g2d = (Graphics2D) g;
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
							  RenderingHints.VALUE_ANTIALIAS_OFF);
		g2d.setColor(new Color(0, 50, 0, 80));
		g2d.fillOval(tl.x, tl.y, d, d);
		g2d.setColor(Color.BLACK);
		g2d.drawOval(tl.x, tl.y, d, d);
	}

	public int diameter() {
		return (int) Math.round(radius()*2);
	}

	public Point topLeftCorner() {
		int r = (int) Math.round(radius());
		Point tl = new Point(center);
		Point vector = new Point(-r, -r);
		MyMath.translate(tl, vector);
		return tl;
	}

	@Override
	public boolean contains(Point p) {
		return MyMath.distanceSquared(center, p) <= radiusSquared();
	}

	@Override
	public void translate(int dx, int dy) {
		Point v = new Point(dx, dy);
		MyMath.translate(center, v);
		MyMath.translate(moon, v);
	}

	private double radius() {
		return MyMath.distance(center, moon);
	}

	private double radiusSquared() {
		return MyMath.distanceSquared(center, moon);
	}

	@Override
	public <R> R accept(ShapeVisitor<R> visitor) {
		return visitor.visit(this);
	}
}
