package shape_editor.shapes;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;

import shape_editor.serializers.ShapeVisitor;

public class Rectangle implements Shape {
	public Point corner1, corner2;

	public Rectangle(Point corner1, Point corner2) {
		this.corner1 = corner1;
		this.corner2 = corner2;
	}

	@Override
	public void paint(Graphics g) { // TODO refactor: remove duplication with the state
		Graphics2D g2d = (Graphics2D) g;
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
							  RenderingHints.VALUE_ANTIALIAS_OFF);
		int width = getWidth();
		int height = getHeight();
		Point topLeftCorner = this.topLeftCorner();
		g2d.setColor(new Color(0, 50, 0, 80));
		g2d.fillRect(topLeftCorner.x, topLeftCorner.y, width, height);
		g2d.setColor(Color.BLACK);
		g2d.drawRect(topLeftCorner.x, topLeftCorner.y, width, height);
	}

	@Override
	public boolean contains(Point p) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void translate(int dx, int dy) {
		// TODO Auto-generated method stub
	}

	@Override
	public <R> R accept(ShapeVisitor<R> visitor) {
		return visitor.visit(this);
	}

	public int getWidth() {
		return Math.abs(corner2.x - corner1.x);
	}

	public int getHeight() {
		return Math.abs(corner2.y - corner1.y);
	}

	public Point topLeftCorner() {
		int topLeftX = Math.min(corner1.x, corner2.x);
		int topLeftY = Math.min(corner1.y, corner2.y);
		return new Point(topLeftX, topLeftY);
	}

}
