package shape_editor.shapes;

import java.awt.Graphics;
import java.awt.Point;

import shape_editor.serializers.ShapeVisitor;

public interface Shape {
	public void paint(Graphics g);
	public boolean contains(Point p);
	public void translate(int dx, int dy);
	public <R> R accept(ShapeVisitor<R> visitor);
}
