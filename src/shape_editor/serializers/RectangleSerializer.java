package shape_editor.serializers;

import java.awt.Point;
import java.util.Scanner;

import shape_editor.shapes.Rectangle;

public class RectangleSerializer implements ShapeSerializer<Rectangle>{

	private static RectangleSerializer instance;

	public static RectangleSerializer getInstance() {
		if (instance == null) {
			instance = new RectangleSerializer();
		}
		return instance;
	}

	@Override
	public String getCode() {
		return "Rectangle";
	}

	@Override
	public String serialize(Rectangle rectangle) {
		return getCode() +
				" " + rectangle.corner1.x + " " + rectangle.corner1.y +
				" " + rectangle.corner2.x + " " + rectangle.corner2.y;
	}

	@Override
	public Rectangle unserialize(String line) {
		try(Scanner scanner = new Scanner(line)) {
			String code = scanner.next();
			if(!code.equals(getCode()))
				throw new RuntimeException("ERROR: corrupted save");

			int corner1X = scanner.nextInt(); // TODO extract and refactor the two points
			int corner1Y = scanner.nextInt();
			Point corner1 = new Point(corner1X, corner1Y);

			int corner2X = scanner.nextInt();
			int corner2Y = scanner.nextInt();
			Point corner2 = new Point(corner2X, corner2Y);

			return new Rectangle(corner1, corner2);
		}
	}
}
