package shape_editor.serializers;

import java.io.PrintWriter;
import java.util.List;

import shape_editor.shapes.Circle;
import shape_editor.shapes.Rectangle;
import shape_editor.shapes.Shape;

public class ShapeWriter implements ShapeVisitor<String> {
	private PrintWriter writer;

	public ShapeWriter(PrintWriter writer) {
		this.writer = writer;
	}

	@Override
	public String visit(Circle circle) {
		return CircleSerializer.getInstance().serialize(circle);
	}

	@Override
	public String visit(Rectangle rectangle) {
		return RectangleSerializer.getInstance().serialize(rectangle);
	}

	public void write(Shape shape) {
		String line = shape.accept(this);
		writer.println(line);
	}

	public void writeAll(List<Shape> shapes) {
		shapes.forEach(this::write);
	}

	public void close() {
		writer.close();
	}

}