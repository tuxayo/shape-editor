package shape_editor.serializers;

import shape_editor.shapes.Circle;
import shape_editor.shapes.Rectangle;

public interface ShapeVisitor<R> {
	public R visit(Circle circle);
	public R visit(Rectangle rectangle);
}
