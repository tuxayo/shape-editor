package shape_editor.serializers;

import java.awt.Point;
import java.util.Scanner;

import shape_editor.shapes.Circle;

public class CircleSerializer implements ShapeSerializer<Circle> {

	private static CircleSerializer instance;

	public static CircleSerializer getInstance() {
		if (instance == null) {
			instance = new CircleSerializer();
		}
		return instance;
	}

	@Override
	public String getCode() {
		return "Circle";
	}

	@Override
	public String serialize(Circle circle) {
		return getCode() +
				" " + circle.center.x + " " + circle.center.y +
				" " + circle.moon.x + " " + circle.moon.y;
	}

	@Override
	public Circle unserialize(String line) {

		try(Scanner scanner = new Scanner(line)) {

			String code = scanner.next();
			if(!code.equals(getCode()))
				throw new RuntimeException("ERROR: corrupted save");

			int cx = scanner.nextInt(); // TODO extract and refactor the two points
			int cy = scanner.nextInt();
			Point center = new Point(cx, cy);

			int moonX = scanner.nextInt();
			int moonY = scanner.nextInt();
			Point moon = new Point(moonX, moonY);

			return new Circle(center, moon);
		}
	}
}
