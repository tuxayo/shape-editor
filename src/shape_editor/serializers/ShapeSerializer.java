package shape_editor.serializers;

import shape_editor.shapes.Shape;

public interface ShapeSerializer<S extends Shape> {
	public String getCode();
	public String serialize(S shape);
	public S unserialize(String line);
}
