package shape_editor.serializers;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import shape_editor.shapes.Shape;

public class ShapeReader {
	private BufferedReader reader;
	private Map<String, ShapeSerializer<?>> serializers;

	public ShapeReader(BufferedReader reader) {
		this.reader = reader;
		initSerializers();
	}

	private void initSerializers() {
		serializers = new HashMap<>();
		addSerializer(CircleSerializer.getInstance());
		addSerializer(RectangleSerializer.getInstance());
	}

	private void addSerializer(ShapeSerializer<?> serializer) {
		serializers.put(serializer.getCode(), serializer);
	}

	public Shape read() throws IOException {
		String line = reader.readLine();
		if(line == null) return null;
		try (Scanner sc = new Scanner(line)) {
			String code = sc.next();
			ShapeSerializer<?> serializer = serializers.get(code);
			if(serializer == null) {
				throw new RuntimeException("ShapeReader.read() ERROR: serializer not found, corrupted save");
			}
			return serializer.unserialize(line);
		}
	}

	public void readAllList(List<Shape> shapes) throws IOException {
		for(;;) {
			Shape shape = read();
			if(shape == null) break;
			shapes.add(shape);
		}
	}

	public void close() throws IOException {
		reader.close();
	}
}