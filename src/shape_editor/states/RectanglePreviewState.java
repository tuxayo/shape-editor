package shape_editor.states;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;

import shape_editor.ShapePanelContext;
import shape_editor.shapes.Rectangle;
import shape_editor.shapes.Shape;

public class RectanglePreviewState implements ShapeState {
	private Point corner1, corner2;

	public RectanglePreviewState(Point corner1) {
		this.corner1 = corner1;
		this.corner2 = corner1;
	}

	@Override
	public void mousePressed(ShapePanelContext context, Point point) {
	}

	@Override
	public void mouseReleased(ShapePanelContext context, Point point) {
		Shape rectangle = new Rectangle(corner1, corner2);
		context.getShapePanel().add(rectangle);
		context.setState(new RectangleChoosenState());
	}

	@Override
	public void mouseDragged(ShapePanelContext context, Point point) {
		corner2 = point;
		context.getShapePanel().repaint();
	}

	@Override
	public void paint(ShapePanelContext context, Graphics graphics) {
		Rectangle rectangle = new Rectangle(corner1, corner2);
		int width = rectangle.getWidth();
		int height = rectangle.getHeight();
		Point topLeftCorner = rectangle.topLeftCorner();
		graphics.setColor(Color.BLACK);
		graphics.drawRect(topLeftCorner.x, topLeftCorner.y, width, height);
	}
}
