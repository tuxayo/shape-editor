package shape_editor.states;

import java.awt.Graphics;
import java.awt.Point;

import shape_editor.ShapePanelContext;

public interface ShapeState {
	public void mousePressed(ShapePanelContext context, Point point);
	public void mouseReleased(ShapePanelContext context, Point point);
	public void mouseDragged(ShapePanelContext context, Point point);
	public void paint(ShapePanelContext context, Graphics graphics);
}
