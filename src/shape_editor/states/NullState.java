package shape_editor.states;

import java.awt.Graphics;
import java.awt.Point;

import shape_editor.ShapePanelContext;

public class NullState implements ShapeState {

	@Override
	public void mousePressed(ShapePanelContext context, Point point) {
	}

	@Override
	public void mouseReleased(ShapePanelContext context, Point point) {
	}

	@Override
	public void mouseDragged(ShapePanelContext context, Point point) {
	}

	@Override
	public void paint(ShapePanelContext context, Graphics graphics) {
	}

}
