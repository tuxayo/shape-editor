package shape_editor.states;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;

import shape_editor.ShapePanelContext;
import shape_editor.shapes.Circle;
import shape_editor.shapes.Shape;

public class CirclePreviewState implements ShapeState {
	private Point center;
	private Point moon;

	public CirclePreviewState(Point center) { // differ from diagram
		this.center = center;
		this.moon = center;
	}

	@Override
	public void mousePressed(ShapePanelContext context, Point point) {
	}

	@Override
	public void mouseReleased(ShapePanelContext context, Point point) {
		Shape circle = new Circle(center, moon);
		context.getShapePanel().add(circle);
		context.setState(new CircleChoosenState());
	}

	@Override
	public void mouseDragged(ShapePanelContext context, Point point) {
		moon = point;
		context.getShapePanel().repaint(); // TODO test if still usefull
	}

	@Override
	public void paint(ShapePanelContext context, Graphics graphics) {
		Circle circle = new Circle(center, moon);
		Point topLeftCorner = circle.topLeftCorner();
		int diameter = circle.diameter();
		// TODO once it works: check if Graphics2D and RenderingHints are needed
		graphics.setColor(Color.BLACK);
		graphics.drawOval(topLeftCorner.x, topLeftCorner.y, diameter, diameter);
	}
}
