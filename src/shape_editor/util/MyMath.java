package shape_editor.util;

import java.awt.Point;

public class MyMath {
	public static double distance(Point p, Point q) {
		return Math.sqrt(distanceSquared(p, q));
	}

	public static double distanceSquared(Point p, Point q) {
		Point pq = vectorFromTo(p,q);
		return lenghtSquared(pq);
	}

	private static Point vectorFromTo(Point p, Point q) {
		return difference(p, q);
	}

	private static Point difference(Point a, Point b) {
		return new Point(a.x - b.x,
						 a.y - b.y);
	}

	private static double lenghtSquared(Point vector) {
		return dotProduct(vector, vector);
	}

	private static double dotProduct(Point v, Point w) {
		return v.x * w.x
			 + v.y * w.y;
	}

	public static void translate(Point p, Point v) {
		p.x += v.x;
		p.y += v.y;
	}


}